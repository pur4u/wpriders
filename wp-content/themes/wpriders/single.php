<?php get_header(); ?>

    <main class="main single" role="main">
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <!-- post thumbnail -->
                <?php if (has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <div class="post-thumbnail">
                        <?php the_post_thumbnail('full'); ?>
                    </div>
                <?php endif; ?>
                <!-- /post thumbnail -->

                <div class="title-desc">
                    <!-- post title -->
                    <h2>
                        <a href="<?php the_permalink(); ?>"
                           title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                    </h2>
                    <!-- /post title -->

                    <!-- post details -->
                    <div class="excerpt">
                        <?php the_content(); ?>
                    </div>
                    <!-- /post details -->
                </div>

                <?php //edit_post_link(); ?>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>
                <h2><?php _e('Sorry, nothing to display.'); ?></h2>
            </article>
            <!-- /article -->

        <?php endif; ?>

    </main>

<?php get_footer(); ?>