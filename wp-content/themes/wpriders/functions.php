<?php
add_theme_support('post-thumbnails');
add_image_size('post-thumbnail', 1500, 800, true);


function remove_meta_boxs()
{
    remove_meta_box('tagsdiv-post_tag', 'post', 'side');
}

add_action('admin_menu', 'remove_meta_boxs');


// Load styles
add_action('wp_enqueue_scripts', 'styles');
function styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.css', array(), '4.2.0');
    wp_enqueue_style('normalize');

    wp_register_style('style', get_template_directory_uri() . '/style.css', array(), '1.0');
    wp_enqueue_style('style');
}


// Load scripts
function scripts()
{
    wp_register_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0');
    wp_enqueue_script('main');
}

add_action('init', 'scripts');


function register_menu()
{
    register_nav_menus(array(
        'left-menu' => __('Left Menu'),
    ));
}

add_action('init', 'register_menu');


/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/
function create_post_type()
{
    register_taxonomy_for_object_type('category', 'phone-apps'); // Register Taxonomies for Category
    //register_taxonomy_for_object_type('post_tag');
    register_post_type('phone-apps',
        array(
            'labels'       => array(
                'name'               => __('Phone apps'),
                'singular_name'      => __('Phone app'),
                'add_new'            => __('Add New'),
                'add_new_item'       => __('Add New App'),
                'edit'               => __('Edit'),
                'edit_item'          => __('Edit App'),
                'new_item'           => __('New App'),
                'view'               => __('View App'),
                'view_item'          => __('View App'),
                'search_items'       => __('Search'),
                'not_found'          => __('No found apps'),
                'not_found_in_trash' => __('No found apps in Trash')
            ),
            'query_var'    => true,
            'show_ui'      => true,
            'show_in_menu' => true,
            'menu_icon'    => 'dashicons-screenoptions',
            'public'       => true,
            'hierarchical' => true,
            'has_archive'  => true,
            'supports'     => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail'
            ),
            'can_export'   => true,
            'taxonomies'   => array(
                //'post_tag',
                //'category'
                'phone_apps_categories'
            ) // Add Category and Post Tags support
        ));
}

add_action('init', 'create_post_type');


function create_taxonomy()
{
    register_taxonomy_for_object_type('phone_apps_categories', 'phone-apps');
    register_taxonomy(
        'phone_apps_categories',
        'phone-apps',
        array(
            'hierarchical'      => true,
            'label'             => 'Category Apps',  //Display name
            'query_var'         => 'apps',
            'show_ui'           => true,
            'public'            => true,
            'show_admin_column' => true,
            'has_archive'       => true,
            'rewrite'           => array(
                'slug'       => 'apps',
                'with_front' => false, // Don't display the category base before
            )
        )
    );
}

add_action('init', 'create_taxonomy');


/*------------------------------------*\
    Functions
\*------------------------------------*/

function left_nav()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'left-menu',
            'menu'            => '',
            'container'       => 'div',
            'container_class' => 'menu-container left-menu',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul>%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        )
    );
}


/*------------------------------------*\
    Flush
\*------------------------------------*/
function prefix_flush_rewrite_rules()
{
    flush_rewrite_rules();
}

add_action('after_switch_theme', 'prefix_flush_rewrite_rules');

?>