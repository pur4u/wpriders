jQuery(function () {
    jQuery('.left-menu ul li a').click(function (e) {
        if (jQuery('body').hasClass('home')) {
            e.preventDefault();
            var href = $(this).attr('href');
            var array = href.split('/');
            var lastSegment = array[array.length - 1];
            var lastSegment2 = array[array.length - 2];

            if (lastSegment.trim()) {
                var last = lastSegment;
            } else if (lastSegment2.trim()) {
                var last = lastSegment2;
            } else {
                var last = '';
            }

            jQuery('.main article').hide();
            if (!last.trim()) {
                jQuery('.main article').fadeIn(1000);
            } else {
                jQuery('.main article').each(function () {
                    if (jQuery(this).hasClass('phone_apps_categories-' + last)) {
                        jQuery(this).fadeIn(1000);
                    } else {
                        jQuery(this).fadeOut(1000);
                    }
                });
            }

            if (history.pushState) {
                history.pushState(null, null, href); // URL is now /inbox/N
            }
            return false;
        }
        return true;
    });

    var popped = ('state' in window.history && window.history.state !== null), initialURL = location.href;
    jQuery(window).bind('popstate', function (event) {
        // Ignore inital popstate that some browsers fire on page load
        var initialPop = !popped && location.href == initialURL
        popped = true
        if (initialPop) return;
    });


    jQuery('#nav-icon').click(function () {
        $(this).toggleClass('open');
        $('nav').slideToggle('slow');
    });
});


