<html <?php language_attributes(); ?> class="no-js">
<html lang="en">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if (wp_title('', false)) {
            echo ' : ';
        } ?><?php bloginfo('name'); ?></title>

    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>"
          href="<?php bloginfo('rss2_url'); ?>"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">


    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!-- wrapper -->
<div class="wrapper">

    <!-- header -->
    <header class="header clear" role="banner">

        <!-- logo -->
        <div class="logo">
            <a href="<?php echo home_url(); ?>">
                <?php bloginfo('name'); ?>
            </a>
            <p class="subheading"><?php bloginfo('description'); ?></p>
        </div>
        <!-- /logo -->

        <!-- nav -->
        <div class="menu-hamburger">
            <div id="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <nav class="nav" role="navigation">
            <?php left_nav(); ?>
        </nav>
        <!-- /nav -->

    </header>
    <!-- /header -->