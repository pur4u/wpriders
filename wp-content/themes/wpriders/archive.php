<?php get_header(); ?>

    <main class="main" role="main">

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <!-- post thumbnail -->
                <?php if (has_post_thumbnail()) : // Check if thumbnail exists ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="post-thumbnail">
                        <?php the_post_thumbnail('post-thumbnail'); ?>
                    </a>
                <?php endif; ?>
                <!-- /post thumbnail -->

                <div class="title-desc">
                    <!-- post title -->
                    <h2>
                        <a href="<?php the_permalink(); ?>"
                           title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                    </h2>
                    <!-- /post title -->

                    <!-- post details -->
                    <div class="excerpt">
                        <?php the_excerpt(); ?>
                    </div>
                    <!-- /post details -->
                </div>
                <div class="like">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/like.png" alt="Like" height="18" width="18">
                </div>

                <?php //edit_post_link(); ?>

            </article>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <article>
                <h2><?php _e('Sorry, nothing to display.'); ?></h2>
            </article>
            <!-- /article -->

        <?php endif; ?>

    </main>

<?php get_footer(); ?>