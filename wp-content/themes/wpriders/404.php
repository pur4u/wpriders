<?php get_header(); ?>

    <main class="main main-404" role="main">
            <!-- article -->
            <article id="post-404">
                <h1>404! Page not found</h1>
                <h2>
                    <a href="<?php echo home_url(); ?>">Return home?</a>
                </h2>
            </article>
            <!-- /article -->
    </main>

<?php get_footer(); ?>